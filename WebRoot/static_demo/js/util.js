{
	var dataJson ;
	var allType = [ "StringType", "TimestampType", "DateType", "CharacterType",
			"ManyToOneType", "IntegerType", "decimal", "OneToOneType" ];
	var allFrameName = [ 'cascadeSele', 'int', 'date', 'decimal',
			'singleStrselectFrame', 'multiStrselectFrame', 'radioInupt' ,'checkInupt'];
//	var currentType = '';
//	var currentOpr = '';
	var currentData = [];
//	var errorDom;
	var currentMessage = '';
	var errorMessage = [ '第一个条件没有选择', '约束条件没有选择', '请选择值或输入值', '第一个输入值为空',
			'第二个输入值为空', '请选择单选框','复选框值不得为空' ];
	var lastJson = [];
	var tx;
	var te;
	var old_value = "";
	var highlightindex = -1; // 高亮
}
/**
 * 根据传过来的下拉值确定列的类型
 * 
 * @param propertyValue
 *            形式为: Usr.id 为 别名.属性名
 * @returns {*} 下拉中所选的值的类型
 *          "StringType","TimestampType","DateType","CharacterType","ManyToOneType","int","decimal"
 */
var getColumType = function(propertyValue) {
	var type = '';
	var str = propertyValue.split(".");
	var aliaName = str[0];
	var columName = str[1];
	var flag = 0;
	for (var i = 0; i < dataJson.length; i++) {
		var alias = dataJson[i].alias;
		for (var j = 0; j < alias.length; j++) {
			if (aliaName === alias[j]) {
				flag = 1;
				break;
			}
		}
		// 在json中找到了别名
		if (flag === 1) {
			var colums = dataJson[i].colums;
			for (var m = 0; m < colums.length; m++) {
				var colum = colums[m].propertyName;
				if (columName === colum) {
					type = colums[m].type;
					break;
				}
			}
			break;
		}
	}
	return type;
};

/**
 * 生成一个适用于int ,小数，date类型的约束
 * 
 * @returns {string}
 */
var appendIntConstraint = function() {
	var constSelec = '<select id="op" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onchange="contraintSelect(this,null)">';
	constSelec += '<option value="">-------</option>';
	constSelec += '<option value="=">等于</option>';
	constSelec += '<option value="!=">不等于</option>';
	constSelec += '<option value="<">小于</option>';
	constSelec += '<option value="<=">小于等于</option>';
	constSelec += '<option value=">">大于</option>';
	constSelec += '<option value=">=">大于等于</option>';
	constSelec += '<option value="between">在..范围之内</option>';
	constSelec += '<option value="not between">不在..范围之内</option>';
	constSelec += '<option value="is null">为null</option>';
	constSelec += '<option value="is not null">不为null</option>';
	constSelec += '</select>';
	return constSelec;
};
/**
 * 生成根目录
 * 
 * @returns {string}
 */
var rootVal = function() {
	var rootcondition = '<table id="tableClass" class="tableClass"><tr><td  id="left"><img src="/queryBuilder/image/2.png" style="cursor:pointer" alt="Remove" id="remove" class="remove" />'
			+ '<select style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all"><option value="and">并且</option><option value="or">或</option></select></td>'; // 这是条件选择(or/and)的的模块

	rootcondition += '<td class="seperator"><div id="querystmts" class="querystmts"></div><div><img id="add" class="add" style="cursor:pointer" src="/queryBuilder/image/add.gif" alt="Add" />'
			+ '<input id="addroot" type="button" class="ui-button-text ui-c" style="cursor:pointer"  value="新组"></button></div>'; // 这回形成追加新组的按钮
	rootcondition += '</td></tr></table>';
	return rootcondition;
};

/**
 * 生成根目录下的第一个条件
 * 
 * 
 * @returns {string}/
 */
var firstCondition = function() {
	var statement = '<div id = "everyDiv" class="right"><img src="/queryBuilder/image/2.png" alt="Remove" id="remove" class="remove" />';
	statement +='<input type="checkbox" name ="checkInput" id = "checkBox" value="1"/>';
	statement += '<select id="col" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onchange="changeSelect(this)">';
	statement += '<option  value="">' + '----------' + '</option>';
	var count = dataJson.length;
	for (var i = 0; i < count; i++) {
		//		var tableName = dataJson[i].tableName; // 表名，数组
		var alias = dataJson[i].alias; // 别名，数组
		var colums = dataJson[i].colums; // 列，数组
		for (var j = 0; j < alias.length; j++) {
			statement += '<optgroup label="' + alias[j] + '">';
			for (var m = 0; m < colums.length; m++) {
				if (getColumType(alias[j] + '.' + colums[m].propertyName) === 'ManyToOneType'
						|| getColumType(alias[j] + '.' + colums[m].propertyName) === 'OneToOneType') {
					statement += '<option value="' + alias[j] + '.'
							+ colums[m].propertyName + '.id">'
							+ colums[m].label + '</option>';
				} else {
					statement += '<option value="' + alias[j] + '.'
							+ colums[m].propertyName + '">' + colums[m].label
							+ '</option>';
				}
			}
			statement += '</optgroup>';
		}
	}
	statement += '</select></div>';
	return statement;
};

/**
 * 生成一个级联时使用的连接符
 * 
 * @returns {string}
 */
var appendFkonstraint = function() {
	var constSelec = '<select id="op" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" >';
	constSelec += '<option selected="selected" value="=">等于</option>';
	constSelec += '</select>';

	constSelec += '<select style="cursor:pointer" id="col" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all"  name = "cascadeSele">';
	constSelec += '<option  value="">' + '----------' + '</option>';
	var count = dataJson.length;
	for (var i = 0; i < count; i++) {
		//		var tableName = dataJson[i].tableName; // 表名，数组
		var alias = dataJson[i].alias; // 别名，数组
		var colums = dataJson[i].colums; // 列，数组
		for (var j = 0; j < alias.length; j++) {
			constSelec += '<optgroup label="' + alias[j] + '">';
			for (var m = 0; m < colums.length; m++) {
				constSelec += '<option value="' + alias[j] + '.'
						+ colums[m].propertyName + '">' + colums[m].label
						+ '</option>';
			}
			constSelec += '</optgroup>';
		}
	}

	return constSelec;
};

/**
 * 生成一个约束下拉
 * 
 * @returns {string}
 */
var appendDateConstraint = function() {
	var constSelec = '<select id="op" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onchange="contraintSelect(this,null)">';
	constSelec += '<option value="">-------</option>';
	constSelec += '<option value="=">等于</option>';
	constSelec += '<option value="!=">不等于</option>';
	constSelec += '<option value="<=">小于等于</option>';
	constSelec += '<option value=">=">大于等于</option>';
	constSelec += '<option value="between">在..范围之内</option>';
	constSelec += '<option value="is null">为null</option>';
	constSelec += '<option value="is not null">不为null</option>';
	constSelec += '</select>';
	return constSelec;
};
/**
 * 生成一个针对string类型的一个约束下拉
 * 
 * @returns {string}
 */
var appendStringConstraint = function() {
	var constSelec = '<select id="op" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onchange="contraintSelect(this,null)">';
	constSelec += '<option value="">-------</option>';
	constSelec += '<option value="=">等于</option>';
	constSelec += '<option value="!=">不等于</option>';
	constSelec += '<option value="in">在..之内</option>';
	constSelec += '<option value="not in">不在...之内</option>';
	constSelec += '<option value="is null">为null</option>';
	constSelec += '<option value="is not null">不为null</option>';
	constSelec += '<option value="like">像</option>';
	constSelec += '<option value="not like">不像</option>';
	constSelec += '</select>';
	return constSelec;

};
/**
 * 生成一个适合于radio和ckeckBox形式约束下拉
 * 
 * @returns {string}
 */
var appendRadioConstraint = function() {
	var constSelec = '<select id="op" style="cursor:pointer" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onchange="contraintSelect(this,null)">';
	constSelec += '<option value="">-------</option>';
	constSelec += '<option value="=">等于</option>';
	constSelec += '<option value="!=">不等于</option>';
	constSelec += '<option value="in">在...之内</option>';
	constSelec += '<option value="not in">不在...之内</option>';
	constSelec += '<option value="is null">为null</option>';
	constSelec += '</select>';
	return constSelec;
};

/**
 * 生成一个数字输入框
 * 
 * @returns {string}
 */
var intInputFrame = function() {
	var intInput = '<input id="numInput" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" type="text" style="ime-mode:disabled;" name="int" onpaste="return false;"  onkeypress="keyPress()" />';
	return intInput;
};

/**
 * 生成一个时间输入框
 * 
 * @returns {string}
 */
var dateInputFrame = function() {
	var dateInput = '<input id="dateInput" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" type="date" name ="date" id="date"/>';
	return dateInput;
};

/**
 * 生成一个小数输入框
 * 
 * @returns {string}
 */
var decimalInputFrame = function() {
	var decimInput = '<input id="decimInput" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" type="text" name="decimal"  onkeyup="if(isNaN(value))execCommand(\'undo\')" onafterpaste="if(isNaN(value))execCommand(\'undo\')" />';
	return decimInput;
};
/**
 * 生成一个适合于单个string输入的输入框
 * @param values
 *            提示的值
 * @param lasSelect
 *            回显的值
 * @returns {string}
 */
var stringFrame = function(values, lasSelect) {
	var sele = JSON.stringify(lasSelect);
	var stringInput = '';
	if (lasSelect.length <=0) {
		stringInput = '<input id="singleStrselectFrame" name ="singleStrselectFrame" class ="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onkeyup="onKeyUp(this)" onclick="getComponentValue(this)" />';
	} else {
		stringInput = '<input id="singleStrselectFrame" name ="singleStrselectFrame" class ="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" value="'
				+ lasSelect[0]
				+ '" onkeyup="onKeyUp(this)" onclick="getComponentValue(this)" />';
	}
	stringInput += '<input id="hidden" type="hidden" value=\'' + JSON.stringify(values)
			+ '\'/>';
	return stringInput;
};
/**
 * 生成一个适合于多个string输入的输入框
 * 
 * @param values
 *            提示的值
 * @param lasSelect
 *            回显的值
 * @returns {string}
 */
var mutilStringFrame = function(values, lasSelect) {
	var sele = JSON.stringify(lasSelect);
	var stringInput = '';
	if (sele === 'null') {
		stringInput = '<input id="multiStrselectFrame"  name ="multiStrselectFrame" class ="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" onkeyup="onKeyUp(this)"  onclick="getComponentValue(this)" onkeydown="getKey(this)"/>';
	} else {
		var val = '';
		for (i in lasSelect) {
			val += lasSelect[i] + '; ';
		}
		stringInput = '<input id="multiStrselectFrame" name ="multiStrselectFrame" class ="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all" value="'
				+ val
				+ '" onkeyup="onKeyUp(this)" onclick="getComponentValue(this)" onkeydown="getKey(this)"/>';
	}
	stringInput += '<input id="hidden" type="hidden" value=\'' + JSON.stringify(values)
			+ '\'/>';
	return stringInput;
};
/**
 * 生成一个适合于radio输入的块
 * 
 * @param values
 *            提示的值
 * @param lasSelect
 *            回显的值 JSON格式
 * @returns {string}
 */
var radioiInputFrame = function(values, lasSelect) {
	var radioInput = '';
	if (lasSelect == null) {
		for (var i = 0; i < values.length; i += 2) {

			radioInput += '<input type ="radio" name="radioInupt" id="radio'
					+ i + '" value="' + values[i] + '"/><label for="radio'  + i
					+ '" style="cursor:pointer" >' + values[i + 1] + '</label>';
		}
		return radioInput;
	}

	for (var i = 0; i < values.length; i += 2) {
		if (lasSelect[0] == values[i]) {
			radioInput += '<input type ="radio" name="radioInupt" checked="checked" id="radio'
					+ i
					+ '" value="'
					+ values[i]
					+ '"/><label for="radio'
					+ i
					+ '" style="cursor:pointer" >' + values[i + 1] + '</label>';
		} else {
			radioInput += '<input type ="radio" name="radioInupt" id="radio'
					+ i + '" value="' + values[i] + '"/><label for="radio' + i
					+ '" style="cursor:pointer">' + values[i + 1] + '</label>';
		}
	}
	
	radioInput +='<input id="hidden" type="hidden" value=\'' + JSON.stringify(values)
	+ '\'/>';
	return radioInput;
};

/**
 * 生成一个适合于checkbox输入的块
 *
 * @param values
 *            提示的值
 * @param lasSelect
 *            回显的值 JSON格式
 * @returns {string}
 */
var multiCheckBoxFrame = function(values, lasSelect) {
	var checkBoxInput = '';
	if (lasSelect == null) {
		for (var i = 0; i < values.length; i += 2) {

			checkBoxInput += '<input type ="checkbox" name="checkInupt" id="checkBox'
					+ i
					+ '" value="'
					+ values[i]
					+ '"/><label for="checkBox'
					+ i + '" style="cursor:pointer">' + values[i + 1] + '</label>';
		}
		return checkBoxInput;
	}

	for (var i = 0; i < values.length; i += 2) {
		var j = 0;
		var flag = false;
		for (j; j < lasSelect.length; j++) {
			if (lasSelect[j] == values[i]) {
				flag = true;
				break;
			}
		}
		if (flag == false) {
			checkBoxInput += '<input type ="checkbox" name="checkInupt" id="checkBox'
					+ i
					+ '" value="'
					+ values[i]
					+ '"/><label for="checkBox'
					+ i + '" style="cursor:pointer">' + values[i + 1] + '</label>';
			continue;
		}
		checkBoxInput += '<input type ="checkbox" name="checkInupt" checked="checked" id="checkBox'
				+ i
				+ '" value="'
				+ values[i]
				+ '"/><label for="checkBox'
				+ i
				+ '" style="cursor:pointer">' + values[i + 1] + '</label>';
	}
	checkBoxInput +='<input id="hidden" type="hidden" value=\'' + JSON.stringify(values)+'\'/>';
	return checkBoxInput;
};

/**
 * 当触发查询按钮时所有的鼠标滑过的事件监听
 * 
 * @param data
 *            错误的dom
 * @param message
 *            错误信息
 */
function removeEvent(data) {
	data.removeEventListener("mousemove", showDetail);
	data.removeEventListener("mouseout", hideDetail);
};
/**
 * 为错误的地方添加事件监听
 * 
 * @param data
 *            错误的dom
 * @param message
 *            错误信息
 */
function bindEvent(data) {
	data.addEventListener("mousemove", showDetail);
	data.addEventListener("mouseout", hideDetail);
};
/**
 * 设置错误信息位置
 * 
 * @param event
 */
function showDetail(event) {
	var e = event || window.event;
	var box = document.getElementById("box_detail");
	//	var content = currentData;
	box.innerHTML = currentMessage;
	box.style.display = "block";
	/*
	 * box.style.top = e.clientY - content.offsetTop + "px"; box.style.left =
	 * e.clientX - content.offsetLeft + "px";
	 */
	box.style.top = e.clientY+ "px";
	box.style.left = e.clientX +5 + "px";
};
/**
 * 隐藏错误信息
 * 
 */
function hideDetail() {
	document.getElementById("box_detail").style.display = "none";
};
