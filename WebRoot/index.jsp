<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" import="java.util.*,java.io.*" pageEncoding="utf-8"%>
<%@page language="java" import="java.util.Date" %>
<%@page import="com.queryBuilder.util.DataUtil" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
			DataUtil dataUtil = new DataUtil();
	dataUtil.saveVistor(request);
	
	
 %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>QueryBuilder-首页-queryBuilder</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta name="baidu_union_verify" content="ec2f9f7623b4d49638adecc31b57fbd3">
<meta http-equiv="description" content="This is my page">
<meta name="keywords" content="QueryBuilder,Jquery,Bootstrap,jquery,queryBuilder,高级自定义查询,可视化SQL拼接,优雅的可视化SQL拼接,复杂SQL拼接,queryBuilder-jquery"/>
<meta name="description" content="QueryBuilder,Jquery,Bootstrap,jquery,queryBuilder,高级自定义查询,可视化SQL拼接,优雅的可视化SQL拼接,复杂SQL拼接,queryBuilder-jquery" />
<link rel="shortcut icon" href="<%=path %>/css/images/ico.ico" type="image/x-icon" /> 
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet"
	href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap.min.css">

<!-- 可选的Bootstrap主题文件（一般不用引入） -->
<link rel="stylesheet"
	href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
<link rel="stylesheet"
	href="<%=path %>/css/base.css">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="<%=path %>/js/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="<%=path%>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<style>
.jumbotron{
	margin-top:0px;
	padding-top:0px;
}
.nav_top{
margin-right:auto;
margin-left:auto;
	width:100%;
	height:40px;
	background:#f1f1f1;
	border-bottom:1px #cccccc solid;
	margin-bottom:5px;
	padding-top: 2px;
	padding-right: 15px;
	padding-bottom: 0px;
	padding-left: 15px;
}

.nav_today{
	float:left;width:50%;text-align:left; line-height:40px;
}
.nav_count{
	float:left;width:50%;text-align:right; line-height:40px;
}
.nav_count span{
	margin:0px 3px;
}
.git_code{
	margin-top:40px;
}
.other_div{
	margin-top:40px;
}
.join_home,.add_me{
	margin-top:10px;
}

<!--开源中国插件样式-->
.pro_name a {
	color: #4183c4;
}

.osc_git_title {
	background-color: #d8e5f1;
}

.osc_git_box {
	background-color: #fafafa;
}

.osc_git_box {
	border-color: #ddd;
}

.osc_git_info {
	color: #666;
}

.osc_git_main a {
	color: #4183c4;
}


</style>
</head>

<body>
	<div class="jumbotron" >
	
		<div class="container">
		<div id="nav_top" class="nav_top">
		<div class="nav_today">您好，今天是&nbsp;<fmt:formatDate value="<%=new Date() %>" pattern="yyyy年MM月dd日 HH:mm:ss"/></div>
		<div class="nav_count"><span>共有</span><span class="badge"><%=DataUtil.getVistorCount() %></span><span>人前来踏足</span></div>
	</div>
			<h1>Hello, QueryBuilder-Jquery !</h1>
			<p><p>QueryBuilder是一个基于Web的可视化复杂SQL拼接功能的演示平台，在这个演示平台中，我们将QueryBuilder整合进入一个web项目中展示。您可通过下面的链接进入我们的demo查看演示效果,也可通过下方的链接直接查看源码，本项目为一个开开源项目，正处在开发初期，欢迎有志之士的加入。
			<br/>
			QueryBuilder使用bootstrap作为前端框架，能够兼容各种浏览器、以及收集客户端。
			</p>
			<p>
				<a class="btn btn-primary btn-lg" href="<%=path %>/jumpDemo" role="button">进入demo</a>
			</p>
			<div class="git_code">
				<script
					src='http://git.oschina.net/noworld/QueryBuilder/widget_preview'></script>
			</div>
			<div class="other_div">
				<div class="join_home">欢迎加入我们的QQ群进行技术讨论：
					<a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=4402a722b326b59957b5ae8f641f25cf13797290c443fa9328fa8b75b022d0a0"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="软件开发工作交流群" title="软件开发工作交流群"></a>
				</div>
				<div class="add_me">
					或和我在线交流：
					<a target=blank href=tencent://message/?uin=825338623&Site=aa&Menu=yes><img border="0" SRC=http://wpa.qq.com/pa?p=1:825338623:13 alt="有事请Q我"></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
